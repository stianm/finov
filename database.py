# database.py

import sqlite3

def create_database():
    connection = sqlite3.connect('database/transactions.db')
    cursor = connection.cursor()

    # Create a table for transactions
    cursor.execute('''
    CREATE TABLE transactions (
        id INTEGER PRIMARY KEY,
        account TEXT NOT NULL,
        date TEXT NOT NULL,
        payee TEXT,
        description TEXT,
        income REAL DEFAULT 0,
        outcome REAL DEFAULT 0,
        other_columns TEXT
    );
    ''')

    # Create a table for longevity
    cursor.execute('''
    CREATE TABLE longevity (
        date DATE PRIMARY KEY,
        aom INTEGER
    );
    ''')    

    connection.commit()
    connection.close()

if __name__ == "__main__":
    create_database()
