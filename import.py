# import.py

import os
import sqlite3

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
IMPORT_DIR = os.path.join(BASE_DIR, "import")

def import_files():
    for account in os.listdir(IMPORT_DIR):
        account_dir = os.path.join(IMPORT_DIR, account)
        
        if os.path.isdir(account_dir):
            for file in os.listdir(account_dir):
                # You can add logic here to parse each file format accordingly
                # Once parsed, you can insert into the SQLite database
                pass

def insert_into_database(account, date, payee, description, income, outcome, other_columns):
    connection = sqlite3.connect('database/transactions.db')
    cursor = connection.cursor()

    cursor.execute('''
    INSERT INTO transactions (account, date, payee, description, income, outcome, other_columns)
    VALUES (?, ?, ?, ?, ?, ?, ?)
    ''', (account, date, payee, description, income, outcome, other_columns))

    connection.commit()
    connection.close()

if __name__ == "__main__":
    import_files()