# balances.py

import sqlite3

BALANCES_FILE = "config/balances.txt"

def load_initial_balances():
    with open(BALANCES_FILE, 'r') as file:
        balances = {}
        for line in file:
            account, balance = line.strip().split(',')
            balances[account] = float(balance)
    return balances

def insert_initial_balances(balances):
    connection = sqlite3.connect('database/transactions.db')
    cursor = connection.cursor()

    for account, balance in balances.items():
        cursor.execute('''
        INSERT INTO transactions (account, date, payee, description, income, outcome, other_columns)
        VALUES (?, ?, "Opening Balance", "Initial Balance", ?, 0, "")
        ''', (account, '01.01.2023', balance))

    connection.commit()
    connection.close()

if __name__ == "__main__":
    balances = load_initial_balances()
    insert_initial_balances(balances)