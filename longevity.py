import sqlite3
from datetime import datetime

def connect_db():
    return sqlite3.connect('database/transactions.db')

def get_transactions(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT Date, Outcome FROM Transactions WHERE Outcome > 0 ORDER BY Date ASC")
    spends = cursor.fetchall()
    cursor.execute("SELECT Date, Income FROM Transactions WHERE Income > 0 ORDER BY Date ASC")
    incomes = cursor.fetchall()
    return spends, incomes

def calculate_aom(spends, incomes):
    income_pool = [[datetime.strptime(income[0], '%Y-%m-%d'), income[1]] for income in incomes]
    total_days = 0

    for spend in spends:
        spend_date = datetime.strptime(spend[0], '%Y-%m-%d')
        spend_amount = spend[1]
        while spend_amount > 0 and income_pool:
            income_date, income_amount = income_pool[0]
            if spend_amount >= income_amount:
                days_old = (spend_date - income_date).days
                total_days += days_old * income_amount
                spend_amount -= income_amount
                income_pool.pop(0)
            else:
                days_old = (spend_date - income_date).days
                total_days += days_old * spend_amount
                income_pool[0][1] -= spend_amount
                spend_amount = 0

    total_spent = sum([spend[1] for spend in spends])
    aom = total_days / total_spent if total_spent else 0
    return round(aom)

def store_aom(conn, aom):
    cursor = conn.cursor()
    latest_date = datetime.now().strftime('%Y-%m-%d')
    cursor.execute("INSERT OR REPLACE INTO longevity (date, aom) VALUES (?, ?)", (latest_date, aom))
    conn.commit()

def main():
    conn = connect_db()
    spends, incomes = get_transactions(conn)
    aom = calculate_aom(spends, incomes)
    store_aom(conn, aom)
    conn.close()

if __name__ == "__main__":
    main()
