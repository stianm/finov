This project is a Financial Overview tracker.



``` mermaid

flowchart TD
subgraph deployment-finov

subgraph pod-grafana
Grafana

end

subgraph pod-django
backend-route
Grafana --> backend-route
end

subgraph pod-finov
Import
import/GEN
import/BSU
import/BSE
import/FUR
import/RVT
Config
config/balance.txt
config/payees.txt

Export


YNAB
YNAB/ynab_month-year.csv

main.py
balance.py
import.py
export.py
database.py
payees.py
longevity.py

main.py -..- balance.py & import.py & export.py & database.py & payees.py & longevity.py

Database

database.py --> Database

longevity.py --> Database

Import --> import/GEN & import/BSU & import/BSE & import/FUR & import/RVT
import/GEN & import/BSU & import/BSE & import/FUR & import/RVT --> import.py
import.py ==> Database

Config --> config/payees.txt
config/payees.txt --> payees.py

payees.py --> Database

Config --> config/balance.txt
config/balance.txt --> balance.py

balance.py ==> Database


Database --> Grafana
Database --> export.py
export.py --> Export
Export --> YNAB
YNAB --> YNAB/ynab_month-year.csv
end
end

```












Tools
| Program | Usecase |
|--|--|
| Python | Language |
| SQlite3 | Database |
| os | OS tasks |




Features
| Scenario | Status |
|--|--|
| Import data from bank | Not implemented |
| Import data from revolut | Not implemented |
| Import current value from stocks | Not implemented |
| Store the imported data in SQlite | Not implemented |
| Pull payee information from Description | Not implemented |
| Convert amount into Inflow/Outflow | Not implemented |
| Create and categorize transactions | Not implemented |
| Export data to YNAB datastructure | Not implemented |
| Access database from Django | Not implemented |
| Secure Django from unwanted access | Not implemented |
| more | to come |
